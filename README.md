#### Run project
```
    python -m venv ./.venv
    ./.venv/bin/activate
    pip install poetry
    poetry install
    cd src
    python manage.py migrate
    python manage.py runserver
```


#### Linters
```
    cd src
    flake8
    black .
```

#### Deploy
```
    https://salavat-thesis.herokuapp.com/
```
